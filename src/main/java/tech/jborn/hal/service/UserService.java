package tech.jborn.hal.service;

import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;
import static tech.jborn.hal.HalApplicationAPI.Permissions.*;

@Getter
@Service
public class UserService {
    private Integer userId;
    private List<String> permissions;

    public boolean hasPermission(String permission) {
        return permissions != null && permissions.contains(permission);
    }

    public boolean isAuthorized() {
        return permissions != null && userId != null;
    }

    public void login(String login, String password) {
        if ("orders-manager".equals(login)) {
            userId = 1;
            permissions = asList(VIEW_PRODUCTS, VIEW_ORDERS, EDIT_MY_ORDERS);
        } else if ("refunds-manager".equals(login)) {
            userId = 2;
            permissions = asList(VIEW_PRODUCTS, VIEW_REFUNDS);
        } else if ("manager".equals(login)) {
            userId = 3;
            permissions = asList(VIEW_PRODUCTS, VIEW_REFUNDS, VIEW_ORDERS, EDIT_ALL_ORDERS);
        }
    }

    public void logout() {
        userId = null;
        permissions = null;
    }
}
