package tech.jborn.hal.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tech.jborn.hal.resources.Order;
import tech.jborn.hal.resources.OrderPosition;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;

@Service
@RequiredArgsConstructor
public class OrdersService {
    public List<Order> getOrders() {
        return asList(
                new Order(1, 1),
                new Order(2, 2),
                new Order(3, 3)
        );
    }

    public Optional<Order> getOrderById(Integer orderId) {
        return Optional.of(new Order(orderId, 1));
    }

    public Optional<Order> getOrderByPositionId(Integer orderPositionId) {
        return Optional.of(new Order(1, 1));
    }

    public List<OrderPosition> getOrderPositions(Integer orderId) {
        return asList(
                new OrderPosition(1, "Молоко", 2),
                new OrderPosition(2, "Сыр", 5),
                new OrderPosition(3, "Хлеб", 1)
        );
    }

    public Optional<OrderPosition> getOrderPositionById(Integer orderPositionId) {
        return Optional.of(new OrderPosition(orderPositionId, "Молоко", 2));
    }
}
