package tech.jborn.hal.resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderPosition {
    private Integer positionId;
    private String name;
    private Integer count;
}
