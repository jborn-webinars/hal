package tech.jborn.hal.resources;

import lombok.Data;

@Data
public class LoginRequest {
    private String login;
    private String password;
}
