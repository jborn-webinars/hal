package tech.jborn.hal;

import org.springframework.hateoas.LinkRelation;

public interface HalApplicationAPI {
    interface Permissions {
        String VIEW_PRODUCTS = "VIEW_PRODUCTS";
        String VIEW_REFUNDS = "VIEW_REFUNDS";

        String VIEW_ORDERS = "VIEW_ORDERS";
        String EDIT_MY_ORDERS = "EDIT_MY_ORDERS";
        String EDIT_ALL_ORDERS = "EDIT_ALL_ORDERS";
    }

    interface Rels {
        LinkRelation AUTH_LOGIN_REL = LinkRelation.of("auth:login");
        LinkRelation AUTH_LOGOUT_REL = LinkRelation.of("auth:logout");
        LinkRelation USER_PROFILE_REL = LinkRelation.of("auth:user-profile");

        LinkRelation PRODUCTS_REL = LinkRelation.of("products:list");

        LinkRelation REFUNDS_REL = LinkRelation.of("refunds:list");

        LinkRelation ORDERS_REL = LinkRelation.of("orders:list");
        LinkRelation ORDERS_GET_ONE_REL = LinkRelation.of("orders:get-one");
        LinkRelation DELETE_ORDER_POSITION_REL = LinkRelation.of("order:position:delete");
        LinkRelation UPDATE_ORDER_POSITION_REL = LinkRelation.of("order:position:update");
    }

    interface Embedded {
        LinkRelation ORDER_POSITIONS = LinkRelation.of("order-positions");
    }

    static <T> T any() {
        return null;
    }
}
