package tech.jborn.hal.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jborn.hal.service.UserService;

import static org.springframework.hateoas.mediatype.hal.HalModelBuilder.halModel;
import static tech.jborn.hal.HalApplicationAPI.Permissions.*;
import static tech.jborn.hal.HalApplicationAPI.Rels.*;
import static tech.jborn.hal.HalApplicationAPI.any;
import static tech.jborn.hal.controllers.AuthController.*;
import static tech.jborn.hal.controllers.OrdersController.getOrderLink;
import static tech.jborn.hal.controllers.OrdersController.getOrdersLink;
import static tech.jborn.hal.controllers.ProductsController.getProductsLink;
import static tech.jborn.hal.controllers.RefundsController.getRefundsLink;

@RequestMapping("/api")
@RestController

@RequiredArgsConstructor
public class WelcomeController {
    private final UserService user;

    @GetMapping
    public RepresentationModel<?> get() {
        HalModelBuilder modelBuilder = halModel();

        if (user.isAuthorized()) {
            modelBuilder.link(getLogoutLink(AUTH_LOGOUT_REL));
            modelBuilder.link(getUserProfileLink(USER_PROFILE_REL));

            if (user.hasPermission(VIEW_PRODUCTS)) {
                modelBuilder.link(getProductsLink(PRODUCTS_REL));
            }

            if (user.hasPermission(VIEW_REFUNDS)) {
                modelBuilder.link(getRefundsLink(REFUNDS_REL));
            }

            if (user.hasPermission(VIEW_ORDERS)) {
                modelBuilder.link(getOrdersLink(ORDERS_REL));
                modelBuilder.link(getOrderLink(any(), ORDERS_GET_ONE_REL));
            }
        } else {
            modelBuilder.link(getLoginLink(AUTH_LOGIN_REL));
        }

        return modelBuilder.build();
    }
}
