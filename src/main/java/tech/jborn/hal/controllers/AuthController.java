package tech.jborn.hal.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jborn.hal.resources.LoginRequest;
import tech.jborn.hal.resources.UserProfile;
import tech.jborn.hal.service.UserService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.ResponseEntity.ok;
import static tech.jborn.hal.HalApplicationAPI.any;

@RequestMapping("/api")
@RestController

@RequiredArgsConstructor
public class AuthController {
    private final UserService user;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest request) {
        user.login(request.getLogin(), request.getPassword());
        return ok().build();
    }

    public static Link getLoginLink(LinkRelation relation) {
        return linkTo(methodOn(AuthController.class).login(any())).withRel(relation);
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout() {
        user.logout();
        return ok().build();
    }

    public static Link getLogoutLink(LinkRelation relation) {
        return linkTo(methodOn(AuthController.class).logout()).withRel(relation);
    }

    @PostMapping("/user-profile")
    public ResponseEntity<UserProfile> userProfile() {
        return ok(new UserProfile(user.getUserId(), user.getPermissions()));
    }

    public static Link getUserProfileLink(LinkRelation relation) {
        return linkTo(methodOn(AuthController.class).userProfile()).withRel(relation);
    }
}
