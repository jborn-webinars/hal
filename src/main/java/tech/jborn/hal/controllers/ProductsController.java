package tech.jborn.hal.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.ResponseEntity.ok;

@RequestMapping("/api/products")
@RestController

@RequiredArgsConstructor
public class ProductsController {
    @GetMapping
    public ResponseEntity<?> getProducts() {
        return ok().build();
    }

    public static Link getProductsLink(LinkRelation relation) {
        return linkTo(methodOn(ProductsController.class).getProducts()).withRel(relation);
    }
}
