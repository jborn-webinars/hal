package tech.jborn.hal.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.*;
import org.springframework.hateoas.mediatype.hal.HalModelBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jborn.hal.exceptions.AccessDeniedException;
import tech.jborn.hal.exceptions.ResourceNotFoundException;
import tech.jborn.hal.resources.Order;
import tech.jborn.hal.resources.OrderPosition;
import tech.jborn.hal.service.OrdersService;
import tech.jborn.hal.service.UserService;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.IanaLinkRelations.SELF;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static tech.jborn.hal.HalApplicationAPI.Embedded.ORDER_POSITIONS;
import static tech.jborn.hal.HalApplicationAPI.Permissions.*;
import static tech.jborn.hal.HalApplicationAPI.Rels.DELETE_ORDER_POSITION_REL;
import static tech.jborn.hal.HalApplicationAPI.Rels.UPDATE_ORDER_POSITION_REL;
import static tech.jborn.hal.HalApplicationAPI.any;

@RequestMapping("/api/orders")
@RestController

@RequiredArgsConstructor
public class OrdersController {
    private final UserService user;
    private final OrdersService orders;

    @GetMapping
    public CollectionModel<RepresentationModel<EntityModel<Order>>> getOrders(@RequestParam(name = "expandPositions") Boolean expandPositions) {
        if (user.hasPermission(VIEW_ORDERS)) {
            return CollectionModel.of(
                                          orders.getOrders()
                                                .stream()
                                                .map(order -> orderEntity(order, expandPositions))
                                                .collect(toList())
                                  )
                                  .add(getOrdersLink(SELF));
        }

        throw new AccessDeniedException("Access Denied!");
    }

    public static Link getOrdersLink(LinkRelation relation) {
        return linkTo(methodOn(OrdersController.class).getOrders(any())).withRel(relation);
    }

    @GetMapping("/{orderId}")
    public RepresentationModel<EntityModel<Order>> getOrder(@PathVariable("orderId") Integer orderId) {
        if (user.hasPermission(VIEW_ORDERS)) {
            return orders.getOrderById(orderId)
                         .map(order -> orderEntity(order, true))
                         .orElseThrow(() -> new ResourceNotFoundException("Could not find order by id " + orderId));
        }

        throw new AccessDeniedException("Access Denied!");
    }

    public static Link getOrderLink(Integer orderId, LinkRelation relation) {
        return linkTo(methodOn(OrdersController.class).getOrder(orderId)).withRel(relation);
    }

    public RepresentationModel<EntityModel<Order>> orderEntity(Order order, boolean expandPositions) {
        EntityModel<Order> orderEntity = EntityModel.of(order)
                                                    .add(getOrderLink(order.getOrderId(), SELF));

        HalModelBuilder builder = HalModelBuilder.halModelOf(orderEntity);

        if (expandPositions) {
            List<OrderPosition> orderPositions = orders.getOrderPositions(order.getOrderId());
            builder.embed(
                    orderPositions.stream()
                                  .map(orderPosition -> orderPositionEntity(order, orderPosition)),
                    ORDER_POSITIONS
            );
        }

        return builder.build();
    }

    @GetMapping("/position/{positionId}")
    public RepresentationModel<EntityModel<OrderPosition>> getOrderPosition(@PathVariable("positionId") Integer positionId) {
        if (user.hasPermission(VIEW_ORDERS)) {
            Order order = orders.getOrderByPositionId(positionId)
                                .orElseThrow(() -> new ResourceNotFoundException("Could not find order by positin id " + positionId));

            return orders.getOrderPositionById(positionId)
                         .map(orderPosition -> orderPositionEntity(order, orderPosition))
                         .orElseThrow(() -> new ResourceNotFoundException("Could not find order position by id " + positionId));
        }

        throw new AccessDeniedException("Access Denied!");
    }

    public static Link getOrderPositionLink(Integer positionId, LinkRelation relation) {
        return linkTo(methodOn(OrdersController.class).getOrderPosition(positionId)).withRel(relation);
    }

    public RepresentationModel<EntityModel<OrderPosition>> orderPositionEntity(Order order, OrderPosition orderPosition) {
        EntityModel<OrderPosition> orderPositionEntity = EntityModel.of(orderPosition)
                                                                    .add(getOrderPositionLink(orderPosition.getPositionId(), SELF));

        if ((user.hasPermission(EDIT_MY_ORDERS) && user.getUserId()
                                                       .equals(order.getOwnerId()))
                || user.hasPermission(EDIT_ALL_ORDERS)) {
            orderPositionEntity.add(getUpdateOrderPositionLink(orderPosition.getPositionId(), UPDATE_ORDER_POSITION_REL));
            orderPositionEntity.add(getDeleteOrderPositionLink(orderPosition.getPositionId(), DELETE_ORDER_POSITION_REL));
        }

        return orderPositionEntity;
    }

    @PostMapping("/position/{positionId}")
    public EntityModel<OrderPosition> updateOrderPosition(@PathVariable("positionId") Integer positionId,
                                                          @RequestBody OrderPosition position) {
        // todo
        return null;
    }

    public static Link getUpdateOrderPositionLink(Integer positionId, LinkRelation relation) {
        return linkTo(methodOn(OrdersController.class).updateOrderPosition(positionId, any())).withRel(relation);
    }

    @DeleteMapping("/position/{positionId}")
    public ResponseEntity<?> deleteOrderPosition(@PathVariable("positionId") Integer positionId) {
        // todo
        return null;
    }

    public static Link getDeleteOrderPositionLink(Integer positionId, LinkRelation relation) {
        return linkTo(methodOn(OrdersController.class).deleteOrderPosition(positionId)).withRel(relation);
    }
}
